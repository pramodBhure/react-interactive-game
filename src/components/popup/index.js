import React from 'react';
import "./popup.css";
import { Link } from 'react-router-dom';
import Sadface from '../../images/sad-smiley-face.jpg';
import Happyface from '../../images/happy-smiley-face.jpg';

function Popup(props) {
    // const createClassName=(questionType)=>{
    //     let qt = questionType.replace(" ", "_");
    //     return  qt.toLowerCase();
    // }
    let popupType = props.type;
    const getStarted = ()=>{
        props.resetQuestionType();
        props.startTimer();
    }
    const showSpinnerResult = ()=>{
        return(
            <div className="popup_content">
                {/* <div className={`selected_question_image ${createClassName(props.questionType)}`}> */}
                <div className="selected_question_image">
                    <img src={require('../../images/'+props.questionType.ImageURL).default} />
                </div>
                <div className="selected_question">
                    <h4>You Spun:</h4>
                    <h2>{props.questionType.Category}</h2>
                    <Link to="/questions" className="button" onClick={getStarted}>Get Started</Link>
                </div>
            </div>
        )
    }
    const setGameProgress = ()=>{
        props.setGameProgress();
    }
    const showQuizSuccessMessage = ()=> {
        return (
            <div className="popup_content quiz_result_message">
                <h2>Great!</h2>
                <h4>You have successfully completed the Level</h4>
                <Link to="/" className="button" onClick={setGameProgress}>Play Next Level</Link>
            </div>
        )
    }
    const showQuizFailedMessage = ()=>{
        return (
            <div className="popup_content quiz_result_message">
                <h2>Oh No!</h2>
                <h4>You have not completed the Level</h4>
                <a onClick={props.tryAgain} className="button">Try Again!</a>
            </div>
        )
    }
    const showEndGameMessage = ()=>{
        return (
            <div className="popup_content quiz_result_message">
                <img src={Sadface} alt="Sad"/>
                <h2>Oh No!</h2>
                <h4>You have loosed the game!</h4>
                <Link to="/" onClick={props.replayGame} className="button">Replay</Link>
            </div>
        )
    }
    const showCompletedQuizMessage = ()=>{
        return (
            <div className="popup_content quiz_full_message">
                <img src={Happyface} alt="Sad"/>
                <h2>Congrats!</h2>
                <h4>You have successfully completed the quiz!</h4>
                <Link to="/" onClick={props.replayGame} className="button">Replay</Link>
            </div>
        )
    }
    return (
        <div className="popup_container">
            {(popupType == "showSpinResult") && showSpinnerResult()}
            {(popupType == "showQuizResult" && props.quizResult) && showQuizSuccessMessage()}
            {(popupType == "showQuizResult" && !props.quizResult) && showQuizFailedMessage()}
            {(popupType == "showEndGameMessage") && showEndGameMessage()}
            {(popupType == "showCompletedQuizMessage") && showCompletedQuizMessage()}
        </div>
    );
}

export default Popup;