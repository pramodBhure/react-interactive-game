import React, { useState, useEffect } from 'react';

const Timer = (props) => {
  const {timer, endTheGame} = props;
  const [seconds, setSeconds] = useState(timer);
  const [isActive, setIsActive] = useState(false);

  if(props.isStartTime && !isActive){
    setIsActive(true);
  }
  if(!props.isStartTime && isActive){
    setSeconds(timer);
    setIsActive(false);
  }

  useEffect(() => {
    let interval = null;
    if(isActive){
        interval = setInterval(() => {
        setSeconds(seconds => seconds - 1);
        }, 1000);
    }
    if (seconds == 0) {
      clearInterval(interval);
      endTheGame();
    }
    return () => clearInterval(interval);
  }, [isActive, seconds]);

  return (
    <div className="timer_container">
        <div className="label">Time: </div>
        <div className="timer">
            {seconds}
        </div>
    </div>
  );
};

export default Timer;