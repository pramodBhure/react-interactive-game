import React from 'react';
import "./footer.css";
import Timer from "./timer";

function Footer(props) {
    const renderChances = () => {
        let renderHtml = "";
        console.log('total:', props.chances.total);
        for(let i=0; i<props.chances.total; i++) {
            let chancesleft = props.chances.userChances;
            if(i < chancesleft && !props.endGame){
                renderHtml += '<div class="life_ico left"></div>';
            } else {
                renderHtml += '<div class="life_ico"></div>';
            }
        }
        return {__html: renderHtml};
    }
    return (
        <div className="footer">
            <Timer isStartTime={props.isStartTime} timer={props.timer} endTheGame={props.endTheGame} />
            <div className="chances_container">
                <div className="label">Chances: </div>
                <div className="lifespan" dangerouslySetInnerHTML={renderChances()}></div>
            </div>
        </div>
    );
}

export default Footer;