import React from "react";
import Wheel from "../../components/wheel";
import Popup from "../../components/popup";

function Home(props) {
	const { categories, gameData, showQuestionType, setQuestionType, selectedQuestionType, resetQuestionType, gameProgress } = props;


	return (
		<div>
			<div className="game_progress_container">
				<div className="game_progress">
					<div className="label">Game Progress:</div>
					<div className="progress_box">{gameProgress.userProgress}/{gameProgress.total}</div>
				</div>
			</div>
			{showQuestionType && <Popup questionType={selectedQuestionType} startTimer={props.startTimer} resetQuestionType={resetQuestionType} type="showSpinResult" />}
			<Wheel items={gameData} setQuestionType={setQuestionType} />
			<div className="puzzle_image"></div>
		</div>
	);
}

export default Home;
