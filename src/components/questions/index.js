import React, { Component } from "react";
import "./questions.css";
import { Link, withRouter } from "react-router-dom";
import { Box } from './Box';
import { DropBox } from './DropBox';
import Popup from "../../components/popup";
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

class Questions extends Component {
    constructor() {
		super();
        this.state = {
            lastDroppedItem: null,
            questions: null,
            correct_answers: null,
            recordUserAnswers:[],
            quizResult:false,
            showQuizMessage:false,
            showSuccessMessage:false,
            hideBackButton:true
        }
    }
    componentWillMount(){
        if(this.props.selectedQuestionType){
            this.setState({
                questions: this.props.selectedQuestionType.questions,
                correct_answers: this.props.selectedQuestionType.correct_answers,
            });
        }
    };

    render() {
        if(!this.props.selectedQuestionType){
            this.props.history.push('/');
            return false;
        }
        const {questions} = this.state;
        const handleDrop = (index, item) => {
            let questions = [...this.state.questions];
            let recordUserAnswers = [...this.state.recordUserAnswers];
            // console.log('item: ', item);
            recordUserAnswers[index] = item.data;
            questions[index].lastDroppedItem = item;
            if(item){
                this.setState({
                    questions,
                    recordUserAnswers
                });
            };
        };
        const checkAnswers = () => {
            let correct_answers = [...this.state.correct_answers];
            let recordUserAnswers = [...this.state.recordUserAnswers];
            const isEqual = (a, b) => JSON.stringify(a) === JSON.stringify(b);
            let result = isEqual(correct_answers, recordUserAnswers);
            let questions = [...this.state.questions];
            questions.map((q, index) => {
                questions[index].lastDroppedItem = null
            });
            if(!this.props.quizComplete){
                this.setState({
                    quizResult: result,
                    showQuizMessage: true,
                    questions,
                    recordUserAnswers:[],
                    hideBackButton:false
                })
            } else {
                this.setState({
                    quizResult: result,
                    showQuizMessage: false,
                    showSuccessMessage: true,
                    questions,
                    recordUserAnswers:[],
                    hideBackButton:false
                })
            }
        }
        const tryAgain = () => {
            this.props.removeChanceByOne();
            this.setState({
                showQuizMessage: false,
                hideBackButton:false
            })
        }
		return (
            <DndProvider backend={HTML5Backend}>
                {this.state.showSuccessMessage && <Popup type="showCompletedQuizMessage" replayGame={this.props.replayGame}/> }
                {this.props.endGame && <Popup type="showEndGameMessage" replayGame={this.props.replayGame}/> }
                {(this.state.showQuizMessage && !this.props.quizComplete) && <Popup showQuizMessage={this.state.showQuizMessage} setGameProgress={this.props.setGameProgress} gameProgress={this.props.gameProgress} quizResult={this.state.quizResult} type="showQuizResult" tryAgain={tryAgain} replayGame={this.props.replayGame} /> }
                <div className="questions_container">
                    <div className="questions_list">
                        {
                            questions.map((q, index) =>{
                                return (
                                    <div className="option" id={q.id}>
                                        <div className="question">{q.question}</div>
                                        <DropBox key={q.id} allowedDropEffect="move" id={q.id} lastDroppedItem={q.lastDroppedItem} onDrop={(item) => handleDrop(index, item)}/>
                                    </div>
                                )
                            })
                        }
                        <div>
                            <a onClick={checkAnswers} className="button sm">
                                Submit Answer
                            </a>
                        </div>
                        <div>
                            {this.state.hideBackButton && <Link to="/" className="button sm back_button white">Back</Link>}
                        </div>
                    </div>
                    <div className="image_dragger">
                        {
                            questions.map(q =>{
                                return (
                                    <Box key={q.id} src={require('../../images/'+q.image.path).default} data={q.image.data} id={q.id}/>
                                )
                            })
                        }
                    </div>
                </div>
            </DndProvider>
		);
	}
}

export default withRouter(Questions);