import React from 'react';
import { useDrag } from "react-dnd";
import { ItemTypes } from "./ItemTypes";
import "./questions.css";

export const Box = ({ src, data, id }) => {
	const [{ opacity, isDragging}, drag] = useDrag(
		() => ({
			type: 'box',
			item: { src, data, id },
			end(item, monitor) {
				const dropResult = monitor.getDropResult();
			},
            isDragging(monitor) {
                const item = monitor.getItem();
                return src === item.src;
            },
			collect: (monitor) => ({
				opacity: monitor.isDragging() ? 0.4 : 1,
			}),
		}),
		[src]
	);
	return (
		<div ref={drag} style={{ opacity }} className="drag_img">
			<img src={src}/>
		</div>
	);
};
