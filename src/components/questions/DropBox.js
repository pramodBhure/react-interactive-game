import {memo} from 'react';
import { useDrop } from "react-dnd";
import { ItemTypes } from "./ItemTypes";
import "./questions.css";

function selectBackgroundColor(isActive, canDrop) {
	if (isActive) {
		return "darkgreen";
	} else if (canDrop) {
		return "darkkhaki";
	} else {
		return "#e5dad2";
	}
}
export const DropBox = memo(function DropBox({ lastDroppedItem, accepts: accept, onDrop }) {
	const [{ canDrop, isOver, item }, dropbox] = useDrop(
		() => ({
			accept: ItemTypes.BOX,
			drop: (item) => onDrop(item),
			collect: (monitor) => ({
                item:monitor.getItem(),
				isOver: monitor.isOver(),
				canDrop: monitor.canDrop(),
			}),
		})
	);
    const isActive = isOver && canDrop;
	// console.log('item: ', item);
	// console.log('lastDroppedItem: ', lastDroppedItem);
	const backgroundColor = selectBackgroundColor(isActive, canDrop);
	return (
		<div className="drag" ref={dropbox} style={{ backgroundColor }}>
			{lastDroppedItem? <img src={lastDroppedItem.src} />:''}
		</div>
	);
});
