import React from "react";
import "./wheel.css";

export default class Wheel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: null
    };
    this.selectItem = this.selectItem.bind(this);
  }

  selectItem() {
    if (this.state.selectedItem === null) {
      const selectedItem = Math.floor(Math.random() * this.props.items.length);
      if (this.props.onSelectItem) {
        this.props.onSelectItem(selectedItem);
      }
      this.setState({ selectedItem });
      let scope = this;
      setTimeout(function(){ scope.props.setQuestionType(scope.props.items[selectedItem]) }, 4500);
    } else {
      this.setState({ selectedItem: null });
      setTimeout(this.selectItem, 500);
    }
  }

  // componentDidUpdate(){
  //   this.props.setQuestionType(this.props.items[this.state.selectedItem]);
  // }

  render() {
    const { selectedItem } = this.state;
    const { items } = this.props;

    const wheelVars = {
      "--nb-item": items.length,
      "--selected-item": selectedItem
    };
    const spinning = selectedItem !== null ? "spinning" : "";

    console.log("items:", items);
    console.log("Selected Item:", selectedItem);
    console.log("Selected Item:", this.props.items[selectedItem]);

    return (
      <div className="wheel-container">
        <div
          className={`wheel ${spinning}`}
          style={wheelVars}
          onClick={this.selectItem}
        >
          {items.map((item, index) => (
            <div
              className={`wheel-item wheel_${index}`}
              key={index}
              style={{ "--item-nb": index }}
            >
              <img src={require('../../images/'+item.ImageURL).default} />
            </div>
          ))}
          {/* {items.map((item, index) => (
            <div
              className={`wheel-item wheel_${index}`}
              key={index}
              style={{ "--item-nb": index }}
            >
              {item}
            </div>
          ))} */}
        </div>
      </div>
    );
  }
}
