import React, { Component } from "react";
import "./App.css";
import Home from "./components/home";
import Questions from "./components/questions";
import Footer from "./components/Footer";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import customGameData from './game_data.json';

class App extends Component {
	constructor() {
		super();
		this.state = {
			// categories: ["Binoculars", "Jelly Fish", "Skating Board", "Diving Bag", "Umbrella", "Crab", "Pineapple", "Fish", "Dollar", "Turtle", "Diving Fins", "Hat"],
      chances:{
        userChances: customGameData.settings.totalChancesCount,
        total: customGameData.settings.totalChancesCount
      },
			gameProgress: {
        userProgress:customGameData.settings.userProgress,
        total:customGameData.settings.totalProgressCount
      },
      quizComplete:false,
      endGame:false,
      timer:customGameData.settings.timer,
      startTime:false,
			selectedQuestionType: null,
			showQuestionType: false,
      gameData: customGameData.data
		};
	}

	render() {
		const { selectedQuestionType, showQuestionType, chances, endGame, gameProgress, timer, quizComplete } = this.state;
		const setQuestionType = (selectedQuestionType) => {
			this.setState({
				selectedQuestionType,
				showQuestionType: !showQuestionType,
			});
		};
    const resetQuestionType=()=>{
      this.setState({showQuestionType:false});
    }
    const removeChanceByOne=()=>{
      let chances = this.state.chances;
      if(chances.userChances > 1){
        chances.userChances = chances.userChances - 1;
        this.setState({chances});
      } else {
        this.setState({
          chances,
          endGame:true
        });
      }
    }
    const setGameProgress = () => {
      let gameProgress = this.state.gameProgress;
      if(gameProgress.userProgress < gameProgress.total-1){
        gameProgress.userProgress = gameProgress.userProgress + 1;
        this.setState({gameProgress});
      } else {
        gameProgress.userProgress = gameProgress.userProgress + 1;
        this.setState({
          quizComplete:true,
          startTime:false,
          gameProgress
        });
      }
    }
    const endTheGame = ()=>{
      this.setState({
        endGame:true
      });
    }
    const startTimer=()=>{
      this.setState({
        startTime:true
      });
    }
    const replayGame=()=>{
      this.setState({
        chances:{
          userChances: customGameData.settings.totalChancesCount,
          total: customGameData.settings.totalChancesCount
        },
        gameProgress: {
          userProgress:customGameData.settings.userProgress,
          total:customGameData.settings.totalProgressCount
        },
        quizComplete:false,
        endGame:false,
        timer:customGameData.settings.timer,
        startTime:false,
        selectedQuestionType: null,
        showQuestionType: false
      });
    }
		return (
			<div className="App">
				<div className="game_board">
					<Router>
						<Switch>
							<Route path="/questions" component={() => 
                <Questions 
                  selectedQuestionType={selectedQuestionType} 
                  removeChanceByOne={removeChanceByOne} 
                  endGame={endGame}
                  gameData={this.state.gameData}
                  quizComplete={quizComplete}
                  gameProgress={gameProgress}
                  setGameProgress={setGameProgress}
                  replayGame={replayGame}
                />}>
              </Route>
							<Route path="/" component={() => 
                <Home 
                  // categories={categories}
                  gameData={this.state.gameData}
                  setQuestionType={setQuestionType} 
                  showQuestionType={showQuestionType} 
                  selectedQuestionType={selectedQuestionType}
                  resetQuestionType={resetQuestionType}
                  gameProgress={gameProgress}
                  startTimer={startTimer}
                />}>
              </Route>
						</Switch>
					</Router>
          <Footer chances={chances} endGame={endGame} isStartTime={this.state.startTime} timer={timer} endTheGame={endTheGame}/>
				</div>
			</div>
		);
	}
}

export default App;
